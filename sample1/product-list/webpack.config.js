const HtmlWebpackPlugin= require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');

module.exports={
    mode:'development',
    devServer:{
        port: 8082
    },
    plugins:[
        new ModuleFederationPlugin({
            name:'cart',
            filename:'remoteEntery.js',
            exposes:{
                './cartIndex':'./src/bootstrap.js'
            },
            shared:['faker']
        }),
        new HtmlWebpackPlugin({
            template:'./public/index.html'
        })
    ]
};