import faker from 'faker';
const mount  = (el)=>{
    const cartShow =`you have ${faker.random.number()} items in the cart.`
    el.innerHTML= cartShow;
};

if(process.env.NODE_ENV ==='development'){
    const ele = document.querySelector("#cart-dev");
    if(ele){
        mount(ele);
    }
}
export {mount}
